
/*
|------------------------------|
|           Types              |
|------------------------------|
*/
const types = {
    LOGIN_START: 'LOGIN_START',
    LOGIN_FINISHED: 'LOGIN_FINISHED',
    LOGIN_ERROR: 'LOGIN_ERROR',
    LOGOUT_START: 'LOGOUT_START',
    LOGOUT_FINISHED: 'LOGOUT_FINISHED',
    LOGOUT_ERROR: 'LOGOUT_ERROR',
    FETCH_POSTS_START: 'FETCH_POSTS_START',
    FETCH_POSTS_FINISHED: 'FETCH_POSTS_FINISHED',
    FETCH_POSTS_ERROR: 'FETCH_POSTS_ERROR',
    ADD_POST_START: 'ADD_POST_START',
    ADD_POST_FINISHED: 'ADD_POST_FINISHED',
    ADD_POST_ERROR: 'ADD_POST_ERROR',
  };

/*
|------------------------------|
|           Actions            |
|------------------------------|
*/
const loginSatrt = () => ({
    type: types.LOGIN_START,
});

const loginFinished = (user) => ({
    type: types.LOGIN_FINISHED,
    user,
});

const loginError = (error) => ({
    type: types.LOGIN_ERROR,
    error,
});

const logoutSatrt = () => ({
    type: types.LOGOUT_START,
});

const logoutFinished = () => ({
    type: types.LOGOUT_FINISHED,
    user,
});

const loginError = (error) => ({
    type: types.LOGOUT_ERROR,
    error,
});
