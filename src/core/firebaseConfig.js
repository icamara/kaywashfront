
import  firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyALaxJmlRLpL-8LIS4Hf20LYfWGakHiuQ8",
    authDomain: "kaywash-c2907.firebaseapp.com",
    databaseURL: "https://kaywash-c2907.firebaseio.com",
    projectId: "kaywash-c2907",
    storageBucket: "kaywash-c2907.appspot.com",
    messagingSenderId: "199351964371",
    appId: "1:199351964371:web:0bf00e732d1c15976d7a26",
    measurementId: "G-HN0VYFP89D"
  };
  // Initialize Firebase
  const Firebase = firebase.initializeApp(firebaseConfig);

  export default Firebase;
  //firebase.analytics();