
export const nameValidator = (name, label) => {

    if(!name || name.length <= 0) return 'Le ' + label+ ' ne peut pas être vide!';

    return '';
};

export const passwordValidator = (password) => {

    if(!password || password.length <= 0) return 'Le mot de passe ne peut pas être vide!';

    return '';
};

export const phoneNumberValidator = phoneNumber => {

    if(!phoneNumber ) return 'Le numéro de Téléphone saisi est incorrect';

    return '';
}