import React, { memo } from 'react';
import { TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import { Theme } from '../core/Theme';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import BackButton from '../components/BackButton';
import CustomTextInput from '../components/CustomTextInput';
import CustomButton from '../components/CustomButton';


const RegisterScreen = ({navigation}) => {

    const OnSingUPressed = () =>{

    };

    return (
        <Background>
            <BackButton goBack={() => navigation.navigate('LoginScreen')}></BackButton>
            <Logo />
            <Header>Créer un compte</Header>
            <CustomTextInput
                placeholder="Prénom"
                autoCompleteType='name'
                textContentType='name'
            />
            <CustomTextInput
                placeholder="Nom"
                autoCompleteType='name'
                textContentType='familyName'
            />
            <CustomTextInput
                placeholder="Téléphone"
                autoCompleteType='tel'
                keyboardType='numeric'
                textContentType='telephoneNumber'
            />
            <CustomTextInput
                placeholder="Password"
                secureTextEntry
            />
            <CustomButton mode="outlined" onPress={OnSingUPressed}>
                Créer un compte
            </CustomButton>
            <View style={styles.row}>
                <Text style={styles.label}>Vous n'avez pas encore de compte ? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
                    <Text style={styles.link}>Se connecter</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
};

const styles = StyleSheet.create({
    forgotPassword: {
      width: '100%',
      alignItems: 'flex-end',
      marginBottom: 24,
    },
    row: {
      flexDirection: 'row',
      marginTop: 4,
    },
    label: {
      color: Theme.colors.secondary,
    },
    link: {
      fontWeight: 'bold',
      color: Theme.colors.primary,
    },
  });

export default memo(RegisterScreen);