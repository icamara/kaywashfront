import React, { memo, useState } from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';
import { Theme } from '../core/Theme';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import BackButton from '../components/BackButton';
import CustomTextInput from '../components/CustomTextInput';
import CustomButton from '../components/CustomButton';
import { phoneNumberValidator, passwordValidator } from '../core/utils';
import { authenticate } from '../services/UserService';
import { loginWithFacebook, logout } from '../services/AuthService';

import Firebase  from '../core/firebaseConfig';
//import * as firebase from 'firebase';
import FBLoginButton from '../components/FBLoginButton';
import PhoneAuthScreen  from './PhoneAuthScreen';


const LoginScreen = ({ navigation }) => {

    

    const [phoneNumber, setPhoneNumber] = useState({ value: '', error: '' });
    const [password, setPassword] = useState({ value: '', error: '' });
    const [User, setUser] = useState({ value: null});

    const OnLoginPressed = () => {
        const phoneError = phoneNumberValidator(phoneNumber.value);
        const passwordError = passwordValidator(password.value);

        /* if(phoneError || passwordError){
            setPhoneNumber({...phoneNumber, error: phoneError});
            setPassword({...password, error: passwordError});

            return;
        }

        authenticate(phoneNumber.value, password.value, 'Customer')
        .then((res) => {
            console.log(res);
        }); */

        navigation.navigate('PhoneAuthScreen');
        
    };

    const OnLoginWithFbPressed = () => {

        //logout();

        loginWithFacebook();
        
        // Listen for authentication state to change.
        Firebase.auth().onAuthStateChanged((user) => {
            if (user != null) {
                //user is authenticated

                setUser({user})
                console.log(user);

                navigation.navigate('HomeScreen');
            }
            
            else{
                //user is signout
                navigation.navigate('LoginScreen');
            }
        });
        
    }

    return (
        <Background>
            <BackButton goBack={() => navigation.navigate('WelcomeScreen')}></BackButton>
            <Logo />
            <Header>Connexion</Header>
            <FBLoginButton onPress={OnLoginWithFbPressed}/>
            <View style={styles.row}>
                <Text style={styles.label}>OU</Text>
            </View>
            <CustomTextInput
                placeholder="Téléphone"
                autoCompleteType='tel'
                value={phoneNumber.value}
                keyboardType='numeric'
                textContentType='telephoneNumber'
                onChangeText={text => setPhoneNumber({ value: text, error: '' })}
                errorText={phoneNumber.error}
            />
            <CustomTextInput
                placeholder="Password"
                secureTextEntry
                onChangeText={text => setPassword({ value: text, error: '' })}
                value={password.value}
                errorText={password.error}
            />
            <View style={styles.forgotPassword}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('ForgotPasswordScreen')}
                >
                    <Text style={styles.label}>Mot de passe oublié?</Text>
                </TouchableOpacity>
            </View>

            <CustomButton mode="outlined" onPress={OnLoginPressed}>
                Se connecter
            </CustomButton>

            <View style={styles.row}>
                <Text style={styles.label}>Vous n'avez pas encore de compte ? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
                    <Text style={styles.link}>Créer un compte</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
};

const styles = StyleSheet.create({
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    label: {
        color: Theme.colors.secondary,
    },
    link: {
        fontWeight: 'bold',
        color: Theme.colors.primary,
    },
    fbLoogin: {
        
    },
    text: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 26,
        color: 'white'
    },
});

export default memo(LoginScreen);