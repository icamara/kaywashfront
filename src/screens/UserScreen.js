import React, { memo } from 'react';
import { View, Text } from 'react-native';

const UserScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Mon Compte perso</Text>
        </View>
    )
};

export default memo(UserScreen);