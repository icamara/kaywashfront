import React, { memo } from 'react';
import { View, Text } from 'react-native';

const CarListScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Mes Voitures</Text>
        </View>
    )
};

export default memo(CarListScreen);