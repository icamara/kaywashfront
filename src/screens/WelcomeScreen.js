import React, { memo } from 'react';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Background from '../components/Background';
import CustomButton from '../components/CustomButton';

const WelcomeScreen = ({navigation}) => {
    return (
        <Background>
            <Logo />
            <Header>Accueil</Header>
            <Paragraph>Connectez-vous ou créez un compte.</Paragraph>
            <CustomButton
                mode="outlined"
                onPress={() => navigation.navigate('LoginScreen')}
            >
                Se Connecter
            </CustomButton>
            <CustomButton
                mode="outlined"
                onPress={() => navigation.navigate('RegisterScreen')}
            >
                Créer un Compte
            </CustomButton>
        </Background>
    )
};

export default memo(WelcomeScreen);