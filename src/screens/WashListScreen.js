import React, { memo } from 'react';
import { View, Text } from 'react-native';

const WashListScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Mes Lavages</Text>
        </View>
    )
};

export default memo(WashListScreen);