import React, { memo } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Container, Header, Left, Icon } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import UserScreen from '../screens/UserScreen';
import WashListScreen from '../screens/WashListScreen';
import CarListScreen from '../screens/CarListScreen';
import PaymentScreen from '../screens/PaymentScreen';
import ParameterScreen from '../screens/ParameterScreen'
import CloseButton from '../components/CloseButton';
import Background from '../components/Background';
import WashRequestForm from '../components/WashRequestFrom';

const Home = ({ navigation }) => {

  return (
    <Container>
      <Header>
        <Left>
          <Icon name="ios-menu" onPress={() => navigation.openDrawer()} />
        </Left>
      </Header>
      <WashRequestForm />
    </Container>

  );
}

const Account = ({ navigation }) => {

  return (
    <Background>
      <CloseButton close={() => navigation.navigate('Accueil')}></CloseButton>
      <UserScreen />
    </Background>

  );
}

const WashList = ({ navigation }) => {
  return (
    <Background>
      <CloseButton close={() => navigation.navigate('Accueil')}></CloseButton>
      <WashListScreen />
    </Background>
  );
}

const CarList = ({ navigation }) => {
  return (
    <Background>
      <CloseButton close={() => navigation.navigate('Accueil')}></CloseButton>
      <CarListScreen />
    </Background>
  );
}

const Payment = ({ navigation }) => {
  return (
    <Background>
      <CloseButton close={() => navigation.navigate('Accueil')}></CloseButton>
      <PaymentScreen />
    </Background>
  );
}

const Parameter = ({ navigation }) => {
  return (
    <Background>
      <CloseButton close={() => navigation.navigate('Accueil')}></CloseButton>
      <ParameterScreen />
    </Background>
  );
}

const Drawer = createDrawerNavigator();

const CustomDrawer = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Accueil" component={Home} />
      <Drawer.Screen name="Mon Compte" component={Account} />
      <Drawer.Screen name="Mes Voitures" component={CarList} />
      <Drawer.Screen name="Mes Lavages" component={WashList} />
      <Drawer.Screen name="Paiement" component={Payment} />
      <Drawer.Screen name="Paramètre" component={Parameter} />
    </Drawer.Navigator>
  );
}

const HomeScreen = () => {
  return (
    <NavigationContainer>
      <CustomDrawer />
    </NavigationContainer>
  );
}

export default memo(HomeScreen);

const styles = StyleSheet.create({
  profile: {
    height: 150,
    width: 150,
    borderRadius: 75
  }
})