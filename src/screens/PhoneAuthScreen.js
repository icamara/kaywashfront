import React, { memo, useState } from 'react';
import { FirebaseRecaptchaVerifier, FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import * as firebase from 'firebase/app';
import { StyleSheet, Button } from 'react-native';
import CustomTextInput from '../components/CustomTextInput';
import CustomButton from '../components/CustomButton';
import Background from '../components/Background';


const PhoneAuthScreen = ({ navigation }) => {

    const [state, setState] = useState({
        phoneNumber: '',
        verificationId: '',
        verificationCode: ''
    });
    const [errorCode, setErrorCode] = useState('');

    recaptchaVerifier: FirebaseRecaptchaVerifier;

    onPressSendVerificationCode = async () => {
        console.log(state.phoneNumber);
        const phoneProvider = new firebase.auth.PhoneAuthProvider();
        phoneProvider.verifyPhoneNumber(
            state.phoneNumber,
            recaptchaVerifier
        ).then(function (verifId) {
            console.log(verifId);
            setState({ ...state, verificationId: verifId });
        }).catch(function (error) {
            console.log(error);
        });

    };

    onPressConfirmVerificationCode = async () => {
        const credential = firebase.auth.PhoneAuthProvider.credential(
            state.verificationId,
            state.verificationCode
        );
        firebase.auth().signInWithCredential(credential).then(function (result) {
            console.log(result.user);
            navigation.navigate('HomeScreen');
        }).catch(function (error) {
            console.log(error);
            setErrorCode('Le code de verification est incorrect');
        });
    }

    return (
        <Background >
            <FirebaseRecaptchaVerifierModal
                ref={ref => recaptchaVerifier = ref}
                firebaseConfig={firebase.app().options} />
            <CustomTextInput
                placeholder="Téléphone"
                autoCompleteType='tel'
                value={state.phoneNumber}
                keyboardType='numeric'
                textContentType='telephoneNumber'
                onChangeText={phoneNumber => setState({ ...state, phoneNumber: phoneNumber })}
            />
            <CustomButton mode="outlined" onPress={this.onPressSendVerificationCode}>
                Envoyer le code
            </CustomButton>
            <CustomTextInput
                placeholder="Code Verification"
                value={state.verificationCode}
                keyboardType='numeric'
                onChangeText={verificationCode => setState({ ...state, verificationCode: verificationCode })}
                errorText={errorCode}
            />
            <CustomButton mode="outlined" onPress={this.onPressConfirmVerificationCode}>
                Vérifier le code
            </CustomButton>
        </Background>
    )
};

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4,
    }
});

export default memo(PhoneAuthScreen);