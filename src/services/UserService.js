
var baseURL = 'http://bbc351df.ngrok.io/api/';

export async function authenticate(phoneNumber, password, userType) {

    var route = '';

    if (userType === 'Customer') {
        route = 'Customers/authenticate';
    }
    if (userType === 'CarDetailer') {
        route = 'CarDetailers/authenticate';
    }

    let URL = baseURL + route;

    try {
        const response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: phoneNumber.toString(),
                password: password,
            })
        });
        const responseJson = await response.json();
        return responseJson;
    }
    catch (error) {
        console.error(error);
    }

};

export async function registerCustomer(firstName, lastName, phoneNumber, password){

    let URL = baseURL + 'Customer/register';

    try {
        const response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                phone: phoneNumber.toString(),
                password: password
            })
        });
        const responseJson = await response.json();
        return responseJson;
    }
    catch (error) {
        console.error(error);
    }
};