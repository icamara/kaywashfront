import  * as Facebook from 'expo-facebook';
import { Firebase } from '../core/firebaseConfig';

export async function loginWithFacebook() {

    console.log("login with Facebook....");

    await Facebook.initializeAsync(
        '224992428594516',
    );

    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
        { permissions: ['public_profile'] }
    );

    if (type === 'success') {
        // Build Firebase credential with the Facebook access token.
        const credential = Firebase.auth.FacebookAuthProvider.credential(token);

        // Sign in with credential from the Facebook user.
        await Firebase.auth().signInWithCredential(credential).catch((error) => {
            // Handle Errors here.
        });
    }
}

export async function logout(){
    console.log("logout with Facebook....");
    await Firebase.auth.signOut().catch((error) => {
        // Handle Errors here.
    });
};
