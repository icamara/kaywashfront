import React, { memo } from 'react';
import Background from './Background';
import CustomButton from './CustomButton';
import CustomTextInput from './CustomTextInput';

const WashRequestForm = () => {

    const OnWashRequested = () =>{
        console.log('Demande de lavage');
    }

    return (
        <Background>
            <CustomTextInput
                placeholder="Localisation"
                autoCompleteType='name'
                textContentType='name'
            />
            <CustomButton mode="outlined" onPress={OnWashRequested}>
                Réserver
            </CustomButton>
        </Background>
    );
}

export default memo(WashRequestForm);