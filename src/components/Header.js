import React, { memo } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Theme } from '../core/Theme';

const Header = ({ children }) => {
    return (
        <Text style={styles.header}>{children}</Text>
    )
};

const styles = StyleSheet.create({
    header: {
        fontSize: 26,
        color: Theme.colors.primary,
        fontWeight: 'bold',
        paddingVertical: 14,
    },
});

export default memo(Header);