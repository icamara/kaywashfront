import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const Logo = () => {
    return (
        <Image source={require('../../assets/logo.png')} style={styles.image} />
    )
};

const styles = StyleSheet.create({
    image: {
        width: 150,
        height: 82,
        marginBottom: 12,
    },
});

export default memo(Logo);
