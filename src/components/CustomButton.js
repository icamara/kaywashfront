import React, { memo } from 'react';
import { StyleSheet } from 'react-native';
import { Button as PaperButton } from 'react-native-paper';
import { Theme } from '../core/Theme';

const CustomButton = ({ mode, style, children, ...props }) => {
    return (
        <PaperButton
            style={[
                styles.button,
                mode === 'outlined' && { backgroundColor: Theme.colors.primary },
                style,
            ]}
            labelStyle={styles.text}
            mode={mode}
            {...props}
        >
            {children}
        </PaperButton>
    )
};

const styles = StyleSheet.create({
    button: {
        width: '100%',
        marginVertical: 10,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 26,
        color: 'white'
    },
});

export default memo(CustomButton);