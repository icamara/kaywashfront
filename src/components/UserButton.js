import React, { memo } from 'react';
import { TouchableOpacity, Image, StyleSheet} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';


const UserButton = ({userScreen}) => {
    return (
        <TouchableOpacity onPress={userScreen} style={styles.container}>
            <Image style={styles.image} source={require('../../assets/user_icon.png')}></Image>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 10 + getStatusBarHeight(),
        left: '100%',
      },
      image: {
        width: 24,
        height: 24,
      },
});

export default memo(UserButton);