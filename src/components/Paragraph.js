import React, { memo } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Theme } from '../core/Theme';

const Paragraph = ({ children }) => {
    return (
        <Text style={styles.text}>{children}</Text>
    )
};

const styles = StyleSheet.create({
    text: {
        fontSize: 16,
        lineHeight: 26,
        color: Theme.colors.secondary,
        textAlign: 'center',
        marginBottom: 14,
    },
});

export default memo(Paragraph);