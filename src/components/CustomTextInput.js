import React, { memo } from 'react';
import { Text, View, StyleSheet, TextInput } from 'react-native';
import { Theme } from '../core/Theme';

const CusTomTextInput = ({ errorText, ...props }) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        selectionColor={Theme.colors.primary}
        underlineColor="transparent"
        mode="outlined"
        {...props}
      />
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  input: {
    backgroundColor: Theme.colors.surface,
    padding: 10,
    borderWidth: 1,
  },
  error: {
    fontSize: 14,
    color: Theme.colors.error,
    paddingHorizontal: 4,
    paddingTop: 4,
  },
});


export default memo(CusTomTextInput);