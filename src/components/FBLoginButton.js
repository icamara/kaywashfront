import React, { memo } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const FBLoginButton = ({ ...props }) => {
    return (
        <View>
            <TouchableOpacity
                style={styles.facebookStyle}
                {...props}
            >
                <Image style={styles.image} source={require('../../assets/logo_fb.png')}></Image>
                <View style={styles.SeparatorLine} />
                <View style={styles.container}>
                    <Text style={styles.text}>
                        Connexion Facebook
                </Text>
                </View>

            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    facebookStyle: {
        backgroundColor: '#3a5a9b',
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: .5,
        borderColor: '#fff',
        borderRadius: 5,
        margin: 5,
        height: 30,
        width: 300,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 26,
        color: 'white',
        height: 30,
    },
    image: {
        width: 30,
        height: 30,
    },
    SeparatorLine :{
 
        backgroundColor : '#fff',
        width: 1,
        height: 30
        }
});

export default memo(FBLoginButton);