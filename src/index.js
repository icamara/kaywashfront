import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import {
    WelcomeScreen,
    LoginScreen,
    PhoneAuthScreen,
    RegisterScreen,
    HomeScreen,
    UserScreen
} from './screens';

const Router = createStackNavigator(
    {
        WelcomeScreen,
        LoginScreen,
        PhoneAuthScreen,
        RegisterScreen,
        HomeScreen,
        UserScreen
    },
    {
        initialRouteName: 'WelcomeScreen',
        headerMode: 'none',
    }
);

const App = createAppContainer(Router);

export default App;